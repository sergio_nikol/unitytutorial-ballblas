using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missiles : MonoBehaviour
{

    Queue<GameObject> missilesQueue;

    [SerializeField] private GameObject missilePrefab;
    [SerializeField] private int missilesCount;

    [SerializeField] private float delay = 0.3f;
    [SerializeField] private float speed = 0.3f;

    private GameObject g;
    private float t = 0f;

    #region Singleton class:Missiles
    public static Missiles Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        PrepareMissiles();
    }

    // Update is called once per frame
    void Update()
    {
        t += Time.deltaTime;
        if (t >= delay)
        {
            t = 0f;
            g = SpawnMissile(transform.position);
            g.GetComponent<Rigidbody2D>().velocity = Vector2.up * speed;
        }
    }

    void PrepareMissiles()
    {
        missilesQueue = new Queue<GameObject>();
        for(int i=0; i < missilesCount; i++)
        {
            g = Instantiate(missilePrefab, transform.position, Quaternion.identity, transform);
            g.SetActive(false);
            missilesQueue.Enqueue(g);
        }
    }

    public GameObject SpawnMissile(Vector2 position)
    {
        if (missilesQueue.Count > 0)
        {
            g = missilesQueue.Dequeue();
            g.transform.position = position;
            g.SetActive(true);
            return g;
        }
        
        return null;
    }

    public void DestroyMissile(GameObject missile)
    {
        missilesQueue.Enqueue(missile);
        missile.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("missile"))
        {
            DestroyMissile(collision.gameObject);
        }
    }
}
