using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class Meteor : MonoBehaviour
{

    [SerializeField] protected Rigidbody2D rb;
    [SerializeField] protected int health;
    [SerializeField] protected TMP_Text textHealth;
    [SerializeField] protected float jumpForce;
    protected float[] leftAndRight = new float[2] { -1f, 1f };

    [HideInInspector] public bool isResultOfFission = true;
    protected bool isShowing;
    // Start is called before the first frame update
    void Start()
    {
        UpdateHealthUI();
        isShowing = true;
        rb.gravityScale = 0f;

        if (isResultOfFission)
        {
            FallDown();
        }
        else
        {
            float direction = leftAndRight[Random.Range(0, 2)];
            float screenOffset = Game.Instance.screenWidth * 1.3f;
            transform.position = new Vector2(screenOffset * direction, transform.position.y);
            rb.velocity = new Vector2(-direction, 0f);
            
            
            Invoke("FallDown", Random.Range(screenOffset - 2.5f, screenOffset - 1f));
        }  
    }

    void FallDown()
    {
        isShowing = false;
        rb.gravityScale = 1f;
        rb.AddTorque(Random.Range(-20f, 20f));
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals("cannon"))
        {
            SceneManager.LoadScene(0);
        }
        if(other.tag.Equals("missile"))
        {
            TakeDamage(1);
            Missiles.Instance.DestroyMissile(other.gameObject);
        }
        if (!isShowing && other.tag.Equals("walls"))
        {
            float posX = transform.position.x;
            if (posX > 0)
            {
                rb.AddForce(Vector2.left*150f);
            }
            else
            {
                rb.AddForce(Vector2.right * 150f);
            }
            rb.AddTorque(posX * 4f);
        }
        if (other.tag.Equals("ground"))
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            rb.AddTorque(-rb.angularVelocity * 4f);
        }
    }


    public void TakeDamage(int damage)
    {
        if (health > 1)
        {
            health -= damage;
        }
        else
        {
            Die();
        }
        UpdateHealthUI();
    }

    virtual protected void Die()
    {
        Destroy(gameObject);
    }

    protected void UpdateHealthUI()
    {
        textHealth.text = health.ToString();
    }
}
