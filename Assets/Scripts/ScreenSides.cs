using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenSides : MonoBehaviour
{
    [SerializeField] BoxCollider2D leftWallCollider;
    [SerializeField] BoxCollider2D rightWallCollider;

    private void Awake()
    {
        float screenWidgth = Game.Instance.screenWidth;

        leftWallCollider.transform.position = new Vector3(-screenWidgth-leftWallCollider.size.x/2f,0f,0f);
        rightWallCollider.transform.position = new Vector3(screenWidgth + rightWallCollider.size.x / 2f, 0f, 0f);
        Destroy(this);
    }
}
