using UnityEngine;

public class Cannon : MonoBehaviour
{
    Camera cam;
    Rigidbody2D rb;

    [SerializeField] HingeJoint2D[] wheels;
    JointMotor2D motor;
    
    [SerializeField] float cannonSpeed;
    float screenBounds;
    
    bool isMoving = false;
    
    float velocityX;
    
    Vector2 pos;
    
    
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        rb = GetComponent<Rigidbody2D>();
        pos = rb.position;

        motor = wheels[0].motor;

        screenBounds = Game.Instance.screenWidth - 0.56f;
    }

    // Update is called once per frame
    void Update()
    {
        isMoving = Input.GetMouseButton(0);
        if (isMoving)
        {
            pos.x = cam.ScreenToWorldPoint(Input.mousePosition).x;
        }
    }
    
    void FixedUpdate()
    {
       if(isMoving)
        {
            rb.MovePosition(Vector2.Lerp(rb.position, pos, cannonSpeed * Time.fixedDeltaTime));
            velocityX = pos.x-rb.position.x;
        }
        else{
            rb.velocity = Vector2.zero;
            velocityX = 0f;
        }

        if(Mathf.Abs(velocityX)>0.0f && Mathf.Abs(rb.position.x) < screenBounds)
        {
            motor.motorSpeed = velocityX * 450f;
            MororActive(true);
        }
        else
        {
            motor.motorSpeed = 0;
            MororActive(false);
        }
    }

    void MororActive(bool isActive)
    {
        wheels[0].useMotor = isActive;
        wheels[1].useMotor = isActive;
        wheels[0].motor = motor;
        wheels[1].motor = motor;
    }
}
