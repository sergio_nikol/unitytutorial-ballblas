using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorSpawner : MonoBehaviour
{

    [SerializeField] GameObject[] meteorPrefabs;
    [SerializeField] int meteorCount;
    [SerializeField] float spawnDelay;

    GameObject[] meteors;

    #region Singleton class: MeteorSpawner
    public static MeteorSpawner Instance;
    private void Awake()
    {
            Instance = this;
    }
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        PrepareMeteors();
        StartCoroutine(SpawnMeteors());
    }

    IEnumerator SpawnMeteors()
    {
        for (int i = 0; i < meteorCount; i++)
        {
            meteors[i].SetActive(true);
            yield return new WaitForSeconds(spawnDelay);
        }
    }

    // Update is called once per frame
    void PrepareMeteors()
    {
        meteors = new GameObject[meteorCount];
        int prefabsCount = meteorPrefabs.Length;
        for (int i = 0; i<meteorCount; i++)
        {
            meteors[i] = Instantiate(meteorPrefabs[Random.Range(0, prefabsCount)], transform);
            meteors[i].GetComponent<Meteor>().isResultOfFission = false;
            meteors[i].SetActive(false);
        }
    }
}
